const htmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const path = require('path');

module.exports = {
  entry: path.resolve(__dirname + '/../', 'src/demo/index.js'),
  output: {
    path: path.resolve(__dirname + '/../', 'public')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname + '/../', 'src'),
          path.resolve(__dirname + '/../', 'node_modules/gl-matrix')
        ],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
        options: {
          limit: 10000
        }
      }
    ]
  },
  plugins: [
    new htmlWebpackPlugin({
      template: path.resolve(__dirname + '/../', 'src/demo/index.html'),
      inject: 'body'
    })
  ],
  devServer: {
    port: 3300
  }
};