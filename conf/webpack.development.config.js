const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.base.config');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

const path = require('path');

process.env.NODE_ENV = 'development';

module.exports = merge(common, {
  entry: [
    'webpack-hot-middleware/client?reload=true',
    path.resolve(__dirname + '/../', 'src/demo/index.js')
  ],
  output: {
    filename: 'bundle.js',
    publicPath: '/'
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                importLoaders: 1,
                minimize: false
              }
            },
            'postcss-loader'
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'styles-[hash].css'
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development')
      }
    })
  ]
});