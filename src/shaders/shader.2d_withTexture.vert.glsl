#version 100

attribute vec2 a_positions;
attribute vec2 a_uvs;

uniform mat3 u_matrix;

varying vec2 v_uvs;

void main() {
  gl_Position = vec4((u_matrix * vec3(a_positions, 1)).xy, 0, 1);

  v_uvs = a_uvs;
}
