#version 100

attribute vec2 a_positions;

uniform mat3 u_matrix;

void main() {
  gl_Position = vec4((u_matrix * vec3(a_positions, 1)).xy, 0, 1);
}
