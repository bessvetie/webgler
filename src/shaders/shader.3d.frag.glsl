#version 100

precision mediump float;

varying vec4 v_colors;

uniform vec4 u_colorMult;

void main() {
   gl_FragColor = v_colors * u_colorMult;
}