#version 100

attribute vec4 a_positions;
attribute vec4 a_colors;

uniform mat4 u_matrix;

varying vec4 v_colors;

void main() {
  gl_Position = u_matrix * a_positions;

  v_colors = a_colors;
}
