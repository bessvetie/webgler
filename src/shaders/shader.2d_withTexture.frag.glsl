#version 100

precision mediump float;

varying vec2 v_uvs;

uniform sampler2D u_texture;
uniform vec4 u_color;
uniform vec4 u_colorMult;

void main() {
  gl_FragColor = texture2D(u_texture, v_uvs) * u_color * u_colorMult;
}
