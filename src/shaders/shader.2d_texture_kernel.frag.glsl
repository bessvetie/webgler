#version 100
#define MAX_COUNT 8

precision mediump float;

varying vec2 v_uvs;

uniform int u_imageCount;

uniform sampler2D u_texture;
uniform sampler2D u_textures[MAX_COUNT];
uniform vec2 u_textureSize[MAX_COUNT];

uniform vec4 u_color;
uniform vec4 u_colorMult;

uniform float u_kernel[9];
uniform float u_kernelWeight;

vec4 computeKernel(sampler2D texture, vec2 textureSize);

void main() {
  vec4 texture;

  if(u_imageCount > 0) {
    vec4 prevTexture = vec4(1,1,1,1);
    vec4 kern;

    for(int i = 0; i < MAX_COUNT; i++) {
      if(i >= u_imageCount) {break;}

      kern = computeKernel(u_textures[i], u_textureSize[i]);
      prevTexture = kern * prevTexture;
    }
    texture = prevTexture;
  }
  else {
    texture = texture2D(u_texture, v_uvs);
  }

  gl_FragColor = texture * u_color * u_colorMult;
}

vec4 computeKernel(sampler2D texture, vec2 textureSize) {
  vec2 onePixel = vec2(1.0, 1.0) / textureSize;
  vec4 colorSum =
    texture2D(texture, v_uvs + onePixel * vec2(-1, -1)) * u_kernel[0] +
    texture2D(texture, v_uvs + onePixel * vec2( 0, -1)) * u_kernel[1] +
    texture2D(texture, v_uvs + onePixel * vec2( 1, -1)) * u_kernel[2] +
    texture2D(texture, v_uvs + onePixel * vec2(-1,  0)) * u_kernel[3] +
    texture2D(texture, v_uvs + onePixel * vec2( 0,  0)) * u_kernel[4] +
    texture2D(texture, v_uvs + onePixel * vec2( 1,  0)) * u_kernel[5] +
    texture2D(texture, v_uvs + onePixel * vec2(-1,  1)) * u_kernel[6] +
    texture2D(texture, v_uvs + onePixel * vec2( 0,  1)) * u_kernel[7] +
    texture2D(texture, v_uvs + onePixel * vec2( 1,  1)) * u_kernel[8] ;

  return vec4((colorSum / u_kernelWeight).rgb, 1.0);
}
