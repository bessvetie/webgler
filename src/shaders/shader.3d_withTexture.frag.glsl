#version 100

precision mediump float;

varying vec2 v_uvs;
varying vec4 v_colors;

uniform sampler2D u_texture;
uniform vec4 u_colorMult;

void main() {
   gl_FragColor = texture2D(u_texture, v_uvs) * v_colors * u_colorMult;
}