#version 100

attribute vec4 a_positions;
attribute vec4 a_colors;
attribute vec2 a_uvs;

uniform mat4 u_matrix;

varying vec4 v_colors;
varying vec2 v_uvs;

void main() {
  gl_Position = u_matrix * a_positions;

  v_colors = a_colors;
  v_uvs = a_uvs;
}
