import {WebGLer} from '../lib/WebGLer';

import {Example1} from './example1';
import {Example2} from './example2';
import {Example3} from './example3';
import {Example4} from './example4';

import global from './global.scss';
import scheme from './scheme.scss';
import sidebar_left from './sidebar_left.scss';
import navbar_style from './navbar.scss';

(function() {
  const loaderExamples = {
    Example1,
    Example2,
    Example3,
    Example4
  };

  const container = document.querySelector('#canvas-wrapper');
  const webgler = new WebGLer(container);

  let activeExample = new loaderExamples[`Example${1}`](webgler);

  (function setExample() {
    document.querySelector(`#example${activeExample.id}`).classList.add('active');
    console.log(webgler);

    const sidebarLeft = document.querySelector('.sidebar.left');
    sidebarLeft.addEventListener('click', (event) => {
      const example = event.target.closest('.example');
      if (example) {
        let newExample = null;

        const parent = example.parentElement;
        Array.from(parent.children, (e, i) => {
          if (example === e) {
            e.classList.add('active');
            newExample = i + 1;
          }
          else if (e.classList.contains('active')) {
            e.classList.remove('active');
            activeExample.close(webgler);
          }
        });

        if (activeExample.id !== newExample) {
          activeExample = new loaderExamples[`Example${newExample}`](webgler);
          console.log(webgler);
        }
      }
    });
  }());

  (function setColorMult() {
    const navbar = document.querySelector('.navbar');
    navbar.addEventListener('click', (event) => {
      const colorMult = event.target.closest('#colorMult');
      if (colorMult) {
        if (activeExample.id < 4 && colorMult.checked) {
          webgler.objects[3].setColor({colorMult: [255, 0, 0, 255]});
          webgler.objects[4].setColor({colorMult: [0, 255, 0, 255]});
          webgler.objects[5].setColor({colorMult: [0, 0, 255, 255]});
        }
        else if (activeExample.id === 4 && colorMult.checked) {
          webgler.objects[0].setColor({colorMult: [(Math.random() * 256) | 0, (Math.random() * 256) | 0, (Math.random() * 256) | 0, 255]});
        }
        else {
          const defaultColor = [255, 255, 255, 255];
          webgler.objects.forEach((object) => {
            object.setColor({colorMult: defaultColor});
          });
        }
        webgler.needToRender = true;
      }
    });
  }());

  (function changePrimitiveType() {
    const selector = document.querySelector('#primitive-type-selector');

    selector.addEventListener('change', () => {
      const value = selector.options[selector.selectedIndex].value;
      webgler.objects.forEach((object) => {
        object.setDrawType(value);
      });
      webgler.needToRender = true;
    });
  }());

  (function changeImage() {
    const sidebarRight = document.querySelector('.sidebar.right');

    sidebarRight.addEventListener('change', (event) => {
      let activeImages = [];

      const image = event.target.closest('.image-for-mult');
      if (image) {
        const parent = image.parentElement;
        Array.from(parent.children, (img) => {
          if (img.checked) {
            activeImages.push(`/images/${img.value}`);
          }
        });

        webgler.objects.forEach((object) => {
          object.setTexture(activeImages).then(() => {
            webgler.needToRender = true;
          });
        });
      }
    });
  }());

  (function changeKernel() {
    const selector = document.querySelector('#kernel-edge-selector');

    selector.addEventListener('change', () => {
      const value = selector.options[selector.selectedIndex].value;
      webgler.objects.forEach((object) => {
        object.setKernel(value);
      });
      webgler.needToRender = true;
    });
  }());
}());