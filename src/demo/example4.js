export class Example4 {
  constructor(webgler) {
    this.name = 'Example4';
    this.id = 4;

    this.active = true;

    this.eventResize = () => {
      distributeEvenly();
      webgler.needToRender = true;
    };
    window.addEventListener('resize', this.eventResize);

    const colorMult = document.querySelector('#colorMult');
    colorMult.checked = false;

    const primitiveType = document.querySelector('#primitive-type');
    primitiveType.style.display = 'none';

    const imagesForMult = document.querySelector('#images-for-mult');
    imagesForMult.style.display = 'block';
    const activeImagesChecked = imagesForMult.querySelectorAll('input[name=images-for-mult]:checked');
    let activeImages = [];
    Array.from(activeImagesChecked, (img) => {
      if (img.checked) {
        activeImages.push(`/images/${img.value}`);
      }
    });

    const kernelEdge = document.querySelector('#kernel-edge');
    kernelEdge.style.display = 'block';
    document.querySelector('#kernel-edge-selector').selectedIndex = 0;

    webgler.createProgram('2d', {
      vertex: '../shaders/shader.2d_withTexture.vert.glsl',
      fragment: '../shaders/shader.2d_texture_kernel.frag.glsl'
    });

    const width = 483;
    const height = 538;

    webgler.add('2d', {
      primitiveName: 'Rectangle',
      width: width,
      height: height,
      center: [width / 2, height / 2],
      // colorMult: [0, 0, 255, 255],
      texture: activeImages
    }).then((object) => {
      distributeEvenly();
    });

    function distributeEvenly() {
      const webglerWidth = webgler.canvas.clientWidth;
      const webglerHeight = webgler.canvas.clientHeight;
      webgler.objects[0].translating(webglerWidth / 2, webglerHeight / 2);
    }
  }

  close(webgler) {
    this.active = false;
    webgler.clearProgram();
    window.removeEventListener('resize', this.eventResize);
  }
}
