export class Example3 {
  constructor(webgler) {
    this.name = 'Example3';
    this.id = 3;

    this.active = true;

    this.eventResize = () => {
      distributeEvenly();
      webgler.needToRender = true;
    };
    window.addEventListener('resize', this.eventResize);

    const colorMult = document.querySelector('#colorMult');
    colorMult.checked = false;

    const primitiveType = document.querySelector('#primitive-type');
    primitiveType.style.display = 'none';

    const imagesForMult = document.querySelector('#images-for-mult');
    imagesForMult.style.display = 'none';

    const kernelEdge = document.querySelector('#kernel-edge');
    kernelEdge.style.display = 'none';

    webgler.createProgram('2d', {
      vertex: '../shaders/shader.2d_withTexture.vert.glsl',
      fragment: '../shaders/shader.2d_withTexture.frag.glsl'
    });

    webgler.createProgram('3d', {
      vertex: '../shaders/shader.3d_withTexture.vert.glsl',
      fragment: '../shaders/shader.3d_withTexture.frag.glsl'
    });

    webgler.add('2d', {
      primitiveName: 'Circle',
      radius: 60,
      segments: 40,
      position: [0, 60],
      angle: -90,
      // colorMult: [255, 0, 0, 255],
      texture: '/images/flower.jpg'
    }).then((object) => {
      distributeEvenly();
    });

    webgler.add('2d', {
      primitiveName: 'Triangle',
      data: [
        90, 0,
        0, 120,
        180, 120,
      ],
      position: [-70, 0],
      // colorMult: [0, 255, 0, 255],
      texture: '/images/macaca.jpg'
    }).then((object) => {
      distributeEvenly();
    });

    webgler.add('2d', {
      primitiveName: 'Rectangle',
      width: 100,
      height: 120,
      // colorMult: [0, 0, 255, 255],
      texture: '/images/koira.jpg'
    }).then((object) => {
      distributeEvenly();
    });

    let objectsForRotate = [];
    webgler.add('3d', {
      primitiveName: 'Sphere',
      radius: 60,
      widthSegments: 25,
      heightSegments: 10,
      position: [0, 200, 0],
      // colorMult: [255, 0, 0, 255],
      texture: '/images/color.jpg'
    }).then((object) => {
      distributeEvenly();
      objectsForRotate.push(object);
    });

    webgler.add('3d', {
      primitiveName: 'Cylinder',
      radiusTop: 0,
      radiusBottom: 100,
      height: 120,
      heightSegments: 2,
      radialSegments: 4,
      position: [20, 200, 0],
      // colorMult: [0, 255, 0, 255],
      texture: '/images/macaca.jpg'
    }).then((object) => {
      distributeEvenly();
      objectsForRotate.push(object);
    });

    webgler.add('3d', {
      primitiveName: 'Box',
      width: 100,
      height: 120,
      depth: 200,
      center: [50, 60, 100],
      position: [50, 200, 0],
      size: 60,
      // colorMult: [0, 0, 255, 255],
      texture: '/images/vuxyxol.jpg'
    }).then((object) => {
      distributeEvenly();
      objectsForRotate.push(object);
    });

    function distributeEvenly() {
      const width = webgler.canvas.clientWidth;
      const height = webgler.canvas.clientHeight;

      const numInLineX = 3;
      const numInLineY = 2;
      const segmentsX = width / numInLineX / 2 + 20 * numInLineX;
      const segmentsY = height / numInLineY / 3;

      let x = 0;
      let y = 0;
      webgler.objects.forEach((object, i) => {
        if (!(i % numInLineX)) {
          y += segmentsY;
          x = 0;
        }
        x += segmentsX;
        object.translating(x, y);
      });
    }

    let angle = 0;
    const rotateAnimate = () => {
      if (this.active) {
        angle += 1;
        if (!(angle % 360)) {
          angle = 0;
        }

        objectsForRotate.forEach((object) => {
          object.rotating(angle, angle - 50, angle - 20);
        });

        webgler.needToRender = true;
        requestAnimationFrame(rotateAnimate);
      }
    };
    rotateAnimate();
  }

  close(webgler) {
    this.active = false;
    webgler.clearProgram();
    window.removeEventListener('resize', this.eventResize);
  }
}
