export class Example2 {
  constructor(webgler) {
    this.name = 'Example2';
    this.id = 2;

    this.active = true;

    this.eventResize = () => {
      distributeEvenly();
      webgler.needToRender = true;
    };
    window.addEventListener('resize', this.eventResize);

    const colorMult = document.querySelector('#colorMult');
    colorMult.checked = true;

    const imagesForMult = document.querySelector('#images-for-mult');
    imagesForMult.style.display = 'none';

    const kernelEdge = document.querySelector('#kernel-edge');
    kernelEdge.style.display = 'none';

    const primitiveType = document.querySelector('#primitive-type');
    primitiveType.style.display = 'block';
    const selector = primitiveType.firstElementChild;
    const activePrimitive = selector.options[selector.selectedIndex].value;

    webgler.createProgram('2d', {
      vertex: '../shaders/shader.2d.vert.glsl',
      fragment: '../shaders/shader.2d.frag.glsl'
    });

    webgler.createProgram('3d', {
      vertex: '../shaders/shader.3d.vert.glsl',
      fragment: '../shaders/shader.3d.frag.glsl'
    });

    webgler.add('2d', {
      primitiveName: 'Circle',
      primitiveType: activePrimitive,
      radius: 40,
      segments: 40,
      position: [0, 40],
      color: [255, 0, 0, 255]
    }).then((object) => {
      distributeEvenly();
    });

    webgler.add('2d', {
      primitiveName: 'Triangle',
      primitiveType: activePrimitive,
      data: [
        0, 0,
        0, 80,
        60, 0,
      ],
      color: [0, 255, 0, 255]
    }).then((object) => {
      distributeEvenly();
    });

    webgler.add('2d', {
      primitiveName: 'Rectangle',
      primitiveType: activePrimitive,
      width: 60,
      height: 80,
      color: [0, 0, 255, 255],
    }).then((object) => {
      distributeEvenly();
    });

    let objectsForRotate = [];
    webgler.add('3d', {
      primitiveName: 'Sphere',
      primitiveType: activePrimitive,
      radius: 40,
      widthSegments: 25,
      heightSegments: 10,
      position: [0, 200, 0],
      colorMult: [255, 0, 0, 255]
    }).then((object) => {
      distributeEvenly();
      objectsForRotate.push(object);
    });

    webgler.add('3d', {
      primitiveName: 'Cylinder',
      primitiveType: activePrimitive,
      radiusTop: 0,
      radiusBottom: 60,
      height: 100,
      heightSegments: 2,
      radialSegments: 4,
      position: [0, 200, 0],
      colorMult: [0, 255, 0, 255]
    }).then((object) => {
      distributeEvenly();
      objectsForRotate.push(object);
    });

    webgler.add('3d', {
      primitiveName: 'Box',
      primitiveType: activePrimitive,
      width: 60,
      height: 80,
      depth: 150,
      center: [30, 40, 75],
      position: [30, 200, 0],
      size: 60,
      colorMult: [0, 0, 255, 255]
    }).then((object) => {
      distributeEvenly();
      objectsForRotate.push(object);
    });

    function distributeEvenly() {
      const width = webgler.canvas.clientWidth;
      const height = webgler.canvas.clientHeight;

      const numInLineX = 3;
      const numInLineY = 2;
      const segmentsX = width / numInLineX / 2 + 20 * numInLineX;
      const segmentsY = height / numInLineY / 3;

      let x = 0;
      let y = 0;
      webgler.objects.forEach((object, i) => {
        if (!(i % numInLineX)) {
          y += segmentsY;
          x = 0;
        }
        x += segmentsX;
        object.translating(x, y);
      });
    }

    let angle = 0;
    const rotateAnimate = () => {
      if (this.active) {
        angle += 1;
        if (!(angle % 360)) {
          angle = 0;
        }

        objectsForRotate.forEach((object) => {
          object.rotating(angle, angle - 50, angle - 20);
        });

        webgler.needToRender = true;
        requestAnimationFrame(rotateAnimate);
      }
    };
    rotateAnimate();
  }

  close(webgler) {
    this.active = false;
    webgler.clearProgram();
    window.removeEventListener('resize', this.eventResize);
  }
}
