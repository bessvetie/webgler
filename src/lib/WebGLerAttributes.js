export class WebGLerAttributes {
  constructor(program, gl) {
    let attribSetters = {};

    const numAttribs = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);
    for (let i = 0; i < numAttribs; ++i) {
      const attribInfo = gl.getActiveAttrib(program, i);
      if (!attribInfo) {
        break;
      }
      const index = gl.getAttribLocation(program, attribInfo.name);
      attribSetters[attribInfo.name] = this.getAttributeSetter(gl, index);
    }

    return attribSetters;
  }

  getAttributeSetter(gl, index) {
    return (b) => {
      gl.bindBuffer(gl.ARRAY_BUFFER, b.buffer);
      gl.enableVertexAttribArray(index);

      const {size, type = gl.FLOAT, normalize = false, stride = 0, offset = 0} = b;
      gl.vertexAttribPointer(index, size, type, normalize, stride, offset);
    };
  }
}