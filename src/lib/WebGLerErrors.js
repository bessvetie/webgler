class WebGLerErrors extends Error {
  constructor(message) {
    super(message);
    this.name = "CustomError";

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error()).stack;
    }
  }
}

export class LoadingError extends WebGLerErrors {
  constructor(message) {
    super(message);

    this.name = 'LoadingError';
  }
}

export class CompilationError extends WebGLerErrors {
  constructor(message) {
    super(message);

    this.name = 'CompilationError';
  }
}
