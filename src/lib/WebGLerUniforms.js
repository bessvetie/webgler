export class WebGLerUniforms {
  constructor(program, gl) {
    this.textureUnit = -1;

    const uniformSetters = {};
    const numUniforms = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);

    for (let i = 0; i < numUniforms; i++) {
      const uniformInfo = gl.getActiveUniform(program, i);
      if (!uniformInfo) {
        break;
      }
      let name = uniformInfo.name;
      if (name.substr(-3) === '[0]') {
        name = name.substr(0, name.length - 3);
      }

      uniformSetters[name] = this.getUniformSetter(program, gl, uniformInfo);
    }

    return uniformSetters;
  }

  getUniformSetter(program, gl, uniformInfo) {
    const type = uniformInfo.type;
    const isArray = (uniformInfo.size > 1 && uniformInfo.name.substr(-3) === '[0]');
    const location = gl.getUniformLocation(program, uniformInfo.name);

    switch(type) {
      case gl.FLOAT:
        if (isArray) { return this.setUniform1fv(gl, location); }
        return this.setUniform1f(gl, location);
      case gl.FLOAT_VEC2:
        return this.setUniform2fv(gl, location);
      case gl.FLOAT_VEC3:
        return this.setUniform3fv(gl, location);
      case gl.FLOAT_VEC4:
        return this.setUniform4fv(gl, location);
      case gl.INT:
        if (isArray) { return this.setUniform1iv(gl, location); }
        return this.setUniform1i(gl, location);
      case gl.BOOL:
        return this.setUniform1iv(gl, location);
      case gl.INT_VEC2:
      case gl.BOOL_VEC2:
        return this.setUniform2iv(gl, location);
      case gl.INT_VEC3:
      case gl.BOOL_VEC3:
        return this.setUniform3iv(gl, location);
      case gl.INT_VEC4:
      case gl.BOOL_VEC4:
        return this.setUniform4iv(gl, location);
      case gl.FLOAT_MAT2:
        return this.setUniformMatrix2fv(gl, location);
      case gl.FLOAT_MAT3:
        return this.setUniformMatrix3fv(gl, location);
      case gl.FLOAT_MAT4:
        return this.setUniformMatrix4fv(gl, location);
      case gl.SAMPLER_2D:
        if (isArray) { return this.setUniformT1a(gl, type, uniformInfo.size, location); }
        return this.setUniformT1(gl, type, location);
      case gl.SAMPLER_CUBE:
        if (isArray) { return this.setUniformT1a(gl, type, uniformInfo.size, location); }
        return this.setUniformT1(gl, type, location);
      default:
        throw ('unknown type: 0x' + type.toString(16));
    }
  }

  setUniform1fv(gl, location) {
    return (v) => {
      gl.uniform1fv(location, v);
    };
  }

  setUniform1f(gl, location) {
    return (v) => {
      gl.uniform1f(location, v);
    };
  }

  setUniform2fv(gl, location) {
    return (v) => {
      gl.uniform2fv(location, v);
    };
  }

  setUniform3fv(gl, location) {
    return (v) => {
      gl.uniform2fv(location, v);
    };
  }

  setUniform4fv(gl, location) {
    return (v) => {
      gl.uniform4fv(location, v);
    };
  }

  setUniform1iv(gl, location) {
    return (v) => {
      gl.uniform1iv(location, v);
    };
  }

  setUniform1i(gl, location) {
    return (v) => {
      gl.uniform1i(location, v);
    };
  }

  setUniform2iv(gl, location) {
    return (v) => {
      gl.uniform2iv(location, v);
    };
  }

  setUniform3iv(gl, location) {
    return (v) => {
      gl.uniform3iv(location, v);
    };
  }

  setUniform4iv(gl, location) {
    return (v) => {
      gl.uniform4iv(location, v);
    };
  }

  setUniformMatrix2fv(gl, location) {
    return (v) => {
      gl.uniformMatrix2fv(location, false, v);
    };
  }

  setUniformMatrix3fv(gl, location) {
    return (v) => {
      gl.uniformMatrix3fv(location, false, v);
    };
  }

  setUniformMatrix4fv(gl, location) {
    return (v) => {
      gl.uniformMatrix4fv(location, false, v);
    };
  }

  setUniformT1a(gl, type, size, location) {
    let units = [];
    for (let i = 0; i < size; i++) {
      units.push(this.textureUnit++);
    }

    const bindPoint = WebGLerUniforms.getBindPointForSamplerType(gl, type);

    return (textures) => {
      gl.uniform1iv(location, units);
      textures.forEach((texture, index) => {
        gl.activeTexture(gl.TEXTURE0 + units[index]);
        gl.bindTexture(bindPoint, texture);
      });
    };
  }

  setUniformT1(gl, type, location) {
    this.textureUnit++;

    const bindPoint = WebGLerUniforms.getBindPointForSamplerType(gl, type);

    return (texture) => {
      gl.uniform1i(location, this.textureUnit);
      gl.activeTexture(gl.TEXTURE0 + this.textureUnit);
      gl.bindTexture(bindPoint, texture);
    };
  }

  static getBindPointForSamplerType(gl, type) {
    switch(type) {
      case gl.SAMPLER_2D:
        return gl.TEXTURE_2D;
      case gl.SAMPLER_CUBE:
        return gl.TEXTURE_CUBE_MAP;
      default:
        return undefined;
    }
  }
}