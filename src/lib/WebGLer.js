import * as WebGLerErrors from './WebGLerErrors';
import {WebGLerUniforms} from './WebGLerUniforms';
import {WebGLerAttributes} from './WebGLerAttributes';

import * as WebGLerPrimitives2d from './WebGLerPrimitives2d';
import * as WebGLerPrimitives3d from './WebGLerPrimitives3d';

const WebGLerPrimitives = Object.assign({}, WebGLerPrimitives2d, WebGLerPrimitives3d);

export class WebGLer {
  constructor(container, options = {}) {
    const {autoresizing = true} = options;
    this.animate = options.animate || false;

    this.shaders = {};
    this.programs = {};
    this.objects = [];

    this.canvas = document.createElement('canvas');
    container.appendChild(this.canvas);
    this.getContext();

    if (autoresizing) {
      const autoresize = () => {
        if (this.resize() || this.needToRender || this.animate) {
          this.renderProgram();
          this.needToRender = false;
        }
        requestAnimationFrame(autoresize);
      };
      autoresize();
    }
  }

  getContext() {
    const names = [
      'webgl',
      'experimental-webgl',
      'webkit-3d',
      'moz-webgl'
    ];
    this.gl = null;
    for (let i = 0; i < names.length; i++) {
      try {
        this.gl = this.canvas.getContext(names[i]);
      }
      catch(e) {
        throw new Error(e);
      }

      if (this.gl) {
        break;
      }
    }
  }

  resize() {
    const displayWidth = this.canvas.clientWidth;
    const displayHeight = this.canvas.clientHeight;

    if (this.canvas.width !== displayWidth ||
        this.canvas.height !== displayHeight) {
      this.canvas.width = displayWidth;
      this.canvas.height = displayHeight;
      return true;
    }

    return false;
  }

  loadShaders(programName, options) {
    let promises = [];
    let shaders = {};
    for (let shaderType in options) {
      if (options.hasOwnProperty(shaderType)) {
        let shader = this.loadShader(options[shaderType]);
        shader.then((data) => {
          shaders[shaderType] = data;
        });
        promises.push(shader);
      }
    }

    this.shaders[programName] = Promise.all(promises)
      .then(() => {
        return shaders;
      });
    return this.shaders[programName];
  }

  loadShader(url) {
    const options = {
      method: 'GET',
      mode: 'cors',
      cache: 'default'
    };

    function status(response) {
      if (response.status !== 200) {
        const error = new WebGLerErrors.LoadingError(`${url} File not found or incorrect`);
        error.code = 'NOT_FOUND';
        throw error;
      }

      return response;
    }

    function contentType(response) {
      const contentType = response.headers.get('Content-Type');
      if (!contentType || !contentType.includes('application/octet-stream')) {
        const error = new WebGLerErrors.LoadingError(`${url} The file type doesn't match the expected`);
        error.code = 'WRONG_CONTENT_TYPE';
        throw error;
      }

      return response.text();
    }

    function syntax(response) {
      if (!response.includes('main')) {
        const error = new WebGLerErrors.LoadingError(`${url} The contents of the file doesn't match the expected`);
        error.code = 'INVALID_SINTAX';
        throw error;
      }

      return response;
    }

    const myRequest = new Request(url, options);
    return fetch(myRequest)
      .then(status)
      .then(contentType)
      .then(syntax)
      .then((response) => {
        return `${response}`;
      })
      .catch((response) => {
        if (response.code) {
          console.error(response, response.code);
        }
        else {
          console.error(response);
        }
      });
  }

  createProgram(programName, programShaders) {
    this.loadShaders(programName, programShaders);

    this.programs[programName] = this.shaders[programName]
      .then((shaders) => {
        const compiledShaders = this.compileShaders(shaders);

        const gl = this.gl;
        const program = gl.createProgram();
        for (let type in compiledShaders) {
          if (compiledShaders.hasOwnProperty(type)) {
            const shader = compiledShaders[type];
            gl.attachShader(program, shader);
            gl.deleteShader(shader);
          }
        }
        gl.linkProgram(program);

        const success = gl.getProgramParameter(program, gl.LINK_STATUS);
        if (!success) {
          console.error(new WebGLerErrors.CompilationError(`The compilation of the shader failed: ${gl.getShaderInfoLog(program)}`));
          gl.deleteProgram(program);
          return;
        }

        const uniformSetters = new WebGLerUniforms(program, this.gl);
        const attribSetters = new WebGLerAttributes(program, this.gl);

        return {
          name: programName,
          program,
          uniformSetters,
          attribSetters,
        };
      });

    return this.programs[programName];
  }

  compileShaders(shaders) {
    let compiledShaders = {};

    for (let type in shaders) {
      if (shaders.hasOwnProperty(type)) {
        const shaderType = `${type.toUpperCase()}_SHADER`;
        compiledShaders[shaderType] = this.compileShader(shaderType, shaders[type]);
      }
    }

    return compiledShaders;
  }

  compileShader(shaderType, shaderSource) {
    const gl = this.gl;
    const shader = gl.createShader(gl[shaderType]);
    gl.shaderSource(shader, shaderSource);
    gl.compileShader(shader);

    const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
      console.error(new WebGLerErrors.CompilationError(`The compilation of the shader failed: ${gl.getShaderInfoLog(shader)}`));
      gl.deleteShader(shader);
      return;
    }

    return shader;
  }

  add(programName, options) {
    const object = new WebGLerPrimitives[options.primitiveName](this.gl, null, options);
    this.objects.push(object);
    object.texture.then(() => {
      this.needToRender = true;
    });

    return this.programs[programName]
      .then((programInfo) => {
        object.setProgramm(programInfo);

        this.needToRender = true;

        return object;
      });
  }

  renderProgram() {
    const gl = this.gl;

    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.clearColor(0, 0, 0, 0);

    gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    let lastUsedProgramInfo = null;
    let lastUsedBufferInfo = null;

    this.objects.forEach((object) => {
      if (object._gl.programInfo) {
        const programInfo = object._gl.programInfo;
        const bufferInfo = object._gl.bufferInfo;
        const uniforms = object._gl.uniforms;

        let bindBuffers = false;

        if (programInfo !== lastUsedProgramInfo) {
          lastUsedProgramInfo = programInfo;
          gl.useProgram(programInfo.program);
          bindBuffers = true;
        }

        if (bindBuffers || bufferInfo !== lastUsedBufferInfo) {
          lastUsedBufferInfo = bufferInfo;
          this.setBuffersAndAttributes(programInfo.attribSetters, bufferInfo);
        }

        object._projection(gl.canvas.clientWidth, gl.canvas.clientHeight);
        object._translate();
        object._rotate();
        object._scale();

        object.offsetCenter();

        this.setSetters(programInfo.uniformSetters, uniforms);

        this.drawBufferInfo(bufferInfo, object._gl.primitiveType);
      }
    });
  }

  drawBufferInfo(bufferInfo, primitiveType, count, offset = 0) {
    const gl = this.gl;

    primitiveType = primitiveType || gl.TRIANGLES;

    const numElements = count || bufferInfo.numElements;
    const indices = bufferInfo.indices;
    if (indices) {
      gl.drawElements(primitiveType, numElements, gl.UNSIGNED_SHORT, offset);
    }
    else {
      gl.drawArrays(primitiveType, offset, numElements);
    }
  }

  setBuffersAndAttributes(setters, buffers) {
    const gl = this.gl;
    this.setSetters(setters, buffers.attribs);
    if (buffers.indices) {
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);
    }
  }

  setSetters(setters, values) {
    Object.keys(values).forEach((name) => {
      const setter = setters[name];
      if (setter) {
        setter(values[name]);
      }
    });
  }

  clearProgram() {
    const gl = this.gl;

    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    gl.clearColor(0, 0, 0, 0);

    let lastUsedProgramInfo = null;

    this.objects.forEach((object) => {
      const programInfo = object._gl.programInfo;

      if (programInfo !== lastUsedProgramInfo) {
        lastUsedProgramInfo = programInfo;
        gl.deleteProgram(programInfo.program);
        delete this.shaders[programInfo.name];
        delete this.programs[programInfo.name];
      }
    });

    this.objects = [];
    this.needToRender = true;
  }
}


