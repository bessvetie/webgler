export class WebGLerArrays {
  constructor(gl, arrays, opt_mapping) {
    const bufferInfo = {
      attribs: this.createAttributesFromArrays(gl, arrays, opt_mapping)
    };

    let indices = arrays.indices;
    if (indices) {
      indices = this.makeTypedArray(indices, 'indices');
      bufferInfo.indices = WebGLerArrays.createBufferFromTypedArray(gl, indices, gl.ELEMENT_ARRAY_BUFFER);
      bufferInfo.numElements = indices.length;
    }
    else {
      bufferInfo.numElements = WebGLerArrays.getNumElementsFromNonIndexedArrays(arrays);
    }

    return bufferInfo;
  }

  static getNumElementsFromNonIndexedArrays(arrays) {
    const key = Object.keys(arrays)[0];
    const array = arrays[key];
    if (array.buffer && array.buffer instanceof ArrayBuffer) {
      return array.numElements;
    }
    else {
      return array.data.length / array.numComponents;
    }
  }

  static createBufferFromTypedArray(gl, array, bufferType, drawType) {
    const type = bufferType || gl.ARRAY_BUFFER;
    const usage = drawType || gl.STATIC_DRAW;

    const buffer = gl.createBuffer();
    gl.bindBuffer(type, buffer);
    gl.bufferData(type, array, usage);

    return buffer;
  }

  static getGLTypeForTypedArray(gl, typedArray) {
    if (typedArray instanceof Int8Array) { return gl.BYTE; }
    if (typedArray instanceof Uint8Array) { return gl.UNSIGNED_BYTE; }
    if (typedArray instanceof Int16Array) { return gl.SHORT; }
    if (typedArray instanceof Uint16Array) { return gl.UNSIGNED_SHORT; }
    if (typedArray instanceof Int32Array) { return gl.INT; }
    if (typedArray instanceof Uint32Array) { return gl.UNSIGNED_INT; }
    if (typedArray instanceof Float32Array) { return gl.FLOAT; }
    throw 'unsupported typed array type';
  }

  createAttributesFromArrays(gl, arrays, opt_mapping) {
    const mapping = opt_mapping || this.createMapping(arrays);
    const attribs = {};
    Object.keys(mapping).forEach((attribName) => {
      const bufferName = mapping[attribName];
      const array = this.makeTypedArray(arrays[bufferName], bufferName);
      attribs[attribName] = {
        buffer: WebGLerArrays.createBufferFromTypedArray(gl, array),
        size: array.numComponents,
        type: WebGLerArrays.getGLTypeForTypedArray(gl, array),
        normalize: arrays[bufferName].normalize || false,
      };
    });
    return attribs;
  }

  createMapping(objWithArrays, pref = 'a_') {
    const mapping = {};
    Object.keys(objWithArrays).filter((name) => {
      return name !== 'indices';
    }).forEach((key) => {
      mapping[pref + key] = key;
    });
    return mapping;
  }

  makeTypedArray(array, name) {
    if (array.buffer && array.buffer instanceof ArrayBuffer) {
      return array;
    }

    let type = array.type;
    if (!type && name === "indices") {
      type = Uint16Array;
    }

    let typedArray = this.createAugmentedTypedArray(array.numComponents, array.data.length / array.numComponents | 0, type);
    typedArray.push(array.data);

    return typedArray;
  }

  createAugmentedTypedArray(numComponents, numElements, opt_type) {
    const Type = opt_type || Float32Array;
    const typedArray = new Type(numComponents * numElements);

    let cursor = 0;

    typedArray.push = function() {
      for (let i = 0; i < arguments.length; i++) {
        const value = arguments[i];
        if (Array.isArray(value) || (value.buffer && value.buffer instanceof ArrayBuffer)) {
          for (let j = 0; j < value.length; j++) {
            typedArray[cursor++] = value[j];
          }
        }
        else {
          typedArray[cursor++] = value;
        }
      }
    };
    typedArray.reset = (opt_index) => {
      cursor = opt_index || 0;
    };

    typedArray.numComponents = numComponents;
    Object.defineProperty(typedArray, 'numElements', {
      get: function() {
        return this.length / this.numComponents | 0;
      },
    });
    return typedArray;
  }
}