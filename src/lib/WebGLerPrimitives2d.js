import * as glMatrix from 'gl-matrix';
import {WebGLerPrimitives} from './WebGLerPrimitives';

class WebGLerPrimitive2d extends WebGLerPrimitives {
  constructor(options) {
    super(options);

    this._gl.matrixType = 'mat3';
  }

  resetParameters(options) {
    const color = options.texture ? null : options.color;
    this.setColor({
      color,
      colorMult: options.colorMult
    });
    this.setKernel(options.kernel);
    this.setTexture(options.texture, options.typeTexture);

    const [tx, ty] = options.offset || [];
    this.translating(tx, ty);

    this.rotating(options.angle);

    const [sx, sy] = options.scale || [];
    this.scaling(sx, sy);
  }

  setTexture(image, type = 'any_size') {
    this._gl.uniforms.u_color = WebGLerPrimitives.convertColor([0, 0, 0, 0]);

    super.setTexture(...arguments);

    this.texture.then(() => {
      if (!image || image.length === 0) {
        const color = this.color || WebGLerPrimitives.randomColor();
        this._gl.uniforms.u_color = WebGLerPrimitives.convertColor(color);
      }
      else {
        this._gl.uniforms.u_color = WebGLerPrimitives.convertColor([255, 255, 255, 255]);
      }
    });
    return this.texture;
  }

  translating(x = 0, y = 0) {
    this.offset = {
      x: x,
      y: y
    };
  }

  rotating(angle = 0) {
    this.angle = angle;
  }

  scaling(x = 1, y = 1) {
    this.scale = {
      x: x,
      y: y
    };
  }

  _projection(width, height) {
    glMatrix[this._gl.matrixType].projection(this._gl.uniforms.u_matrix, width, height);
  }

  _rotate() {
    glMatrix[this._gl.matrixType].rotate(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, WebGLerPrimitives.angleToRad(this.angle));
  }
}

export class Triangle extends WebGLerPrimitive2d {
  constructor(gl, programInfo, options) {
    super(options);

    this.position = options.position || [0, 0];
    this.position.push(0);

    const buffers = WebGLerPrimitives.identifyBuffers();
    buffers.positions.data = [
      options.data[0], options.data[1], 0,
      options.data[2], options.data[3], 0,
      options.data[4], options.data[5], 0
    ];
    buffers.uvs.data = [0.5, 0, 0, 0.8, 1, 0.8];
    delete buffers.indices;

    const uniforms = {
      u_matrix: glMatrix[this._gl.matrixType].create(),
    };

    this.setGLVariables(gl, programInfo, buffers, uniforms, options.primitiveType);
    this.resetParameters(options);
  }
}

export class Rectangle extends WebGLerPrimitive2d {
  constructor(gl, programInfo, options) {
    super(options);

    const {
      primitiveType = 'TRIANGLE_STRIP',

      position,
      center,
      color,
      texture,

      width = 1,
      height = 1
    } = options;

    const widthSegments = Math.floor(options.widthSegments) || 1;
    const heightSegments = Math.floor(options.heightSegments) || 1;

    this.position = position || [0, 0, 0];
    this.center = center || [0, 0, 0];

    this.parameters = {
      width: width,
      height: height,
      widthSegments: widthSegments,
      heightSegments: heightSegments
    };

    const buffers = WebGLerPrimitives.identifyBuffers();

    WebGLerPrimitives.buildSide('x', 'y', 'z', width, height, 0, widthSegments, heightSegments, 0, buffers, color);

    const uniforms = {
      u_matrix: glMatrix[this._gl.matrixType].create()
    };

    this.setGLVariables(gl, programInfo, buffers, uniforms, primitiveType);
    this.resetParameters(options);
  }
}

export class Circle extends WebGLerPrimitive2d {
  constructor(gl, programInfo, options) {
    super(options);

    const {
      primitiveType = 'TRIANGLE_FAN',

      position,
      radius = 1,
      thetaStart = 0,
      thetaLength = Math.PI * 2
    } = options;

    const segments = Math.max(3, options.segments);

    this.position = position || [0, 0];

    this.parameters = {
      radius,
      segments,
      thetaStart,
      thetaLength
    };

    const buffers = WebGLerPrimitives.identifyBuffers();

    buffers.positions.data.push(0, 0, 0);
    buffers.normals.data.push(0, 0, 1);
    buffers.uvs.data.push(0.5, 0.5);

    for (let s = 0, i = 3; s <= segments; s++, i += 3) {
      const segment = thetaStart - s / segments * thetaLength;

      let vertex = {};
      vertex.x = radius * Math.cos(segment);
      vertex.y = radius * Math.sin(segment);

      buffers.positions.data.push(vertex.x, vertex.y, 0);
      buffers.indices.data.push(s, s + 1, 0);

      buffers.normals.data.push(0, 0, 1);

      let uv = {};
      uv.x = (buffers.positions.data[i + 1] / radius + 1) / 2;
      uv.y = (-buffers.positions.data[i] / radius + 1) / 2;

      buffers.uvs.data.push(uv.x, uv.y);
    }

    const uniforms = {
      u_matrix: glMatrix[this._gl.matrixType].create(),
    };

    this.setGLVariables(gl, programInfo, buffers, uniforms, primitiveType);
    this.resetParameters(options);
  }
}