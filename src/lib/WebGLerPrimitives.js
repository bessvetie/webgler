import {WebGLerArrays} from './WebGLerArrays';
import * as WebGLerErrors from './WebGLerErrors';
import * as glMatrix from 'gl-matrix';

export class WebGLerPrimitives {
  constructor(options) {
    this.primitiveName = options.primitiveName;
    Object.defineProperties(this, {
      '_gl': {
        value: {}
      }
    });
  }

  setGLVariables(gl, programInfo, buffers, uniforms, primitiveType) {
    const bufferInfo = new WebGLerArrays(gl, buffers);

    this.setProgramm(programInfo);
    this._gl.context = gl;
    this._gl.bufferInfo = bufferInfo;
    this._gl.uniforms = uniforms;
    this._gl.primitiveType = gl[primitiveType];
  }

  setProgramm(programInfo) {
    this._gl.programInfo = programInfo;
  }

  setDrawType(primitiveType) {
    this._gl.primitiveType = this._gl.context[primitiveType];
  }

  setColor(options) {
    const defaultColor = [255, 255, 255, 255];
    const colorMult = options.colorMult || this.colorMult || defaultColor;
    const color = options.color || this.color || defaultColor;

    this._gl.uniforms.u_colorMult = WebGLerPrimitives.convertColor(colorMult);
    if (options.colorMult) {
      this.colorMult = colorMult;
    }

    if (options.color) {
      this.color = color;
    }
  }

  resetTexture() {
    this.setTextureParameters(null, 'any_size');
  }

  setTexture(data, type = 'any_size') {
    const gl = this._gl.context;
    if (!this._gl.uniforms.u_textures) {
      this._gl.uniforms.u_textures = [];
      this._gl.uniforms.u_textureSize = [];
    }

    let images = [].concat(data);
    let textures = [];

    let promises = [];
    for (let i = 0; i < 8; i++) {
      const oldTexture = this._gl.uniforms.u_textures[i];
      if (oldTexture) {
        gl.deleteTexture(oldTexture);
      }
      textures[i] = WebGLerPrimitives.setTextureParameters(gl, null, type, 0);

      let shader = this.loadTexture(images[i])
        .then((image) => {
          gl.deleteTexture(textures[i]);

          if (image) {
            images[i] = image;
            this._gl.uniforms.u_textureSize[i] = [image.width, image.height];
          }
          else {
            this._gl.uniforms.u_textureSize[i] = [1, 1];
          }
          textures[i] = WebGLerPrimitives.setTextureParameters(gl, image, type);
        });
      promises.push(shader);
    }
    this._gl.uniforms.u_texture = textures[0];
    this._gl.uniforms.u_textures = textures;
    this._gl.uniforms.u_imageCount = images.length;

    this.texture = Promise.all(promises)
      .then(() => {
        if (!data || data.length === 0) {
          this._gl.uniforms.u_texture = textures[0];
          this.texture = null;
        }
        else {
          this._gl.uniforms.u_textureSize = [].concat(...this._gl.uniforms.u_textureSize);
          this._gl.uniforms.u_texture = textures[0];
          this._gl.uniforms.u_textures = textures;
          this.texture = images;
        }
      });
    return this.texture;
  }

  loadTexture(image) {
    return new Promise((resolve, reject) => {
      if (!image) {
        resolve();
        return;
      }
      if (image.tagName === 'IMG') {
        resolve(image);
        return;
      }

      const texture = new Image();
      texture.onload = () => {
        resolve(texture);
      };
      texture.onerror = () => {
        reject(new WebGLerErrors.LoadingError(`${image} Texture not found`));
      };
      texture.src = image;
    });
  }

  setKernel(type = 'normal') {
    const kernels = {
      normal: [
        0, 0, 0,
        0, 1, 0,
        0, 0, 0
      ],
      gaussianBlur: [
        0.045, 0.122, 0.045,
        0.122, 0.332, 0.122,
        0.045, 0.122, 0.045
      ],
      gaussianBlur2: [
        1, 2, 1,
        2, 4, 2,
        1, 2, 1
      ],
      gaussianBlur3: [
        0, 1, 0,
        1, 1, 1,
        0, 1, 0
      ],
      unsharpen: [
        -1, -1, -1,
        -1, 9, -1,
        -1, -1, -1
      ],
      sharpness: [
        0, -1, 0,
        -1, 5, -1,
        0, -1, 0
      ],
      sharpen: [
        -1, -1, -1,
        -1, 16, -1,
        -1, -1, -1
      ],
      edgeDetect: [
        -0.125, -0.125, -0.125,
        -0.125, 1, -0.125,
        -0.125, -0.125, -0.125
      ],
      edgeDetect2: [
        -1, -1, -1,
        -1, 8, -1,
        -1, -1, -1
      ],
      edgeDetect3: [
        -5, 0, 0,
        0, 0, 0,
        0, 0, 5
      ],
      edgeDetect4: [
        -1, -1, -1,
        0, 0, 0,
        1, 1, 1
      ],
      edgeDetect5: [
        -1, -1, -1,
        2, 2, 2,
        -1, -1, -1
      ],
      edgeDetect6: [
        -5, -5, -5,
        -5, 39, -5,
        -5, -5, -5
      ],
      sobelHorizontal: [
        1, 2, 1,
        0, 0, 0,
        -1, -2, -1
      ],
      sobelVertical: [
        1, 0, -1,
        2, 0, -2,
        1, 0, -1
      ],
      previtHorizontal: [
        1, 1, 1,
        0, 0, 0,
        -1, -1, -1
      ],
      previtVertical: [
        1, 0, -1,
        1, 0, -1,
        1, 0, -1
      ],
      boxBlur: [
        0.111, 0.111, 0.111,
        0.111, 0.111, 0.111,
        0.111, 0.111, 0.111
      ],
      triangleBlur: [
        0.0625, 0.125, 0.0625,
        0.125, 0.25, 0.125,
        0.0625, 0.125, 0.0625
      ],
      emboss: [
        -2, -1, 0,
        -1, 1, 1,
        0, 1, 2
      ]
    };

    const kernelEdge = ((kernels.hasOwnProperty(type) && kernels[type]) || type);

    this._gl.uniforms.u_kernel = kernelEdge;
    this._gl.uniforms.u_kernelWeight = computeKernelWeight(kernelEdge);

    function computeKernelWeight(edge) {
      const weight = edge.reduce(function(prev, curr) {
        return prev + curr;
      });
      return weight <= 0 ? 1 : weight;
    }
  }

  offsetCenter() {
    if (this.center) {
      const translate = glMatrix[this._gl.matrixType].create();
      glMatrix[this._gl.matrixType].translate(translate, translate, [-this.center[0], -this.center[1], -this.center[2]]);
      glMatrix[this._gl.matrixType].multiply(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, translate);
    }
  }

  _translate() {
    glMatrix[this._gl.matrixType].translate(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, [this.offset.x, this.offset.y, this.offset.z]);

    if (this.position) {
      const translate = glMatrix[this._gl.matrixType].create();
      glMatrix[this._gl.matrixType].translate(translate, translate, [this.position[0], this.position[1], this.position[2]]);
      glMatrix[this._gl.matrixType].multiply(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, translate);
    }
  }

  _scale() {
    glMatrix[this._gl.matrixType].scale(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, [this.scale.x, this.scale.y, this.scale.z]);
  }

  static setTextureParameters(gl, texture, type = 'any_size', alpha = 255) {
    const textureGL = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, textureGL);
    if (!texture) {
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([alpha, alpha, alpha, alpha]));
    }
    else {
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture);
    }

    if (type === 'any_size') {
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    }

    return textureGL;
  }

  static identifyBuffers() {
    return {
      positions: {
        numComponents: 3,
        data: []
      },
      indices: {
        numComponents: 3,
        data: []
      },
      normals: {
        numComponents: 3,
        data: []
      },
      uvs: {
        numComponents: 2,
        data: []
      },
      colors: {
        numComponents: 4,
        data: [],
        type: Uint8Array,
        normalize: true
      }
    }
  }

  static buildSide(u, v, w, sizeW, sizeH, sizeD, gridX, gridY, numberOfVertices, buffers, color, dir = 1) {
    const segmentWidth = sizeW / gridX;
    const segmentHeight = sizeH / gridY;

    const gridX1 = gridX + 1;
    const gridY1 = gridY + 1;

    let vertexCounter = 0;

    for (let iy = 0; iy < gridY1; iy++) {
      const y = sizeH - iy * segmentHeight;

      for (let ix = 0; ix < gridX1; ix++) {
        const x = sizeW - ix * segmentWidth;

        let vertex = {};
        vertex[u] = x;
        vertex[v] = y;
        vertex[w] = sizeD;

        buffers.positions.data.push(vertex.x, vertex.y, vertex.z);

        vertex[u] = 0;
        vertex[v] = 0;
        vertex[w] = sizeD > 0 ? 1 : -1;

        buffers.normals.data.push(vertex.x, vertex.y, vertex.z);

        if (dir > 0) {
          buffers.uvs.data.push(1 - ix / gridX);
          buffers.uvs.data.push(1 - ( iy / gridY ));
        }
        else {
          buffers.uvs.data.push(( iy / gridY ));
          buffers.uvs.data.push(1 - ix / gridX);
        }

        if (color) {
          buffers.colors.data.push(color[0], color[1], color[2], color[3]);
        }

        vertexCounter++;
      }
    }

    for (let iy = 0; iy < gridY; iy++) {
      for (let ix = 0; ix < gridX; ix++) {
        const a = numberOfVertices + ix + gridX1 * iy;
        const d = numberOfVertices + (ix + 1) + gridX1 * iy;
        const b = numberOfVertices + ix + gridX1 * (iy + 1);
        const c = numberOfVertices + (ix + 1) + gridX1 * (iy + 1);

        buffers.indices.data.push(a, b, d);
        buffers.indices.data.push(b, c, d);
      }
    }

    return numberOfVertices + vertexCounter;
  }

  static angleToRad(angle) {
    return angle * Math.PI / 180;
  }

  static convertColor(color) {
    const [r, g, b, a] = color;
    return [r / 255, g / 255, b / 255, a / 255]
  }

  static randomColor() {
    return [(Math.random() * 256) | 0, (Math.random() * 256) | 0, (Math.random() * 256) | 0, 255];
  }
}