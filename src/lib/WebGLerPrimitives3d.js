import * as glMatrix from 'gl-matrix';
import {WebGLerPrimitives} from './WebGLerPrimitives';

class WebGLerPrimitive3d extends WebGLerPrimitives {
  constructor(options) {
    super(options);

    this._gl.matrixType = 'mat4';
  }

  resetParameters(options) {
    const color = options.texture ? null : options.color;
    this.setColor({
      color,
      colorMult: options.colorMult
    });
    this.setTexture(options.texture, options.typeTexture);

    const [tx, ty, tz] = options.offset || [];
    this.translating(tx, ty, tz);

    const [ax, ay, az] = options.angle || [];
    this.rotating(ax, ay, az);

    const [sx, sy, sz] = options.scale || [];
    this.scaling(sx, sy, sz);
  }

  translating(x = 0, y = 0, z = 0) {
    this.offset = {
      x: x,
      y: y,
      z: z
    };
  }

  rotating(angleX = 0, angleY = 0, angleZ = 0) {
    this.angle = {
      x: angleX,
      y: angleY,
      z: angleZ
    };
  }

  scaling(x = 1, y = 1, z = 1) {
    this.scale = {
      x: x,
      y: y,
      z: z
    };
  }

  _projection(right, bottom, left = 0, top = 0, near = 400, far = -400) {
    glMatrix[this._gl.matrixType].ortho(this._gl.uniforms.u_matrix, left, right, bottom, top, near, far);
  }

  _rotate() {
    glMatrix[this._gl.matrixType].rotateX(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, WebGLerPrimitives.angleToRad(this.angle.x));
    glMatrix[this._gl.matrixType].rotateY(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, WebGLerPrimitives.angleToRad(this.angle.y));
    glMatrix[this._gl.matrixType].rotateZ(this._gl.uniforms.u_matrix, this._gl.uniforms.u_matrix, WebGLerPrimitives.angleToRad(this.angle.z));
  }
}

export class Box extends WebGLerPrimitive3d {
  constructor(gl, programInfo, options) {
    super(options);

    const {
      primitiveType,

      position,
      center,
      color,
      texture,

      width = 1,
      height = 1,
      depth = 1
    } = options;

    const widthSegments = Math.floor(options.widthSegments) || 1;
    const heightSegments = Math.floor(options.heightSegments) || 1;
    const depthSegments = Math.floor(options.depthSegments) || 1;

    this.position = position || [0, 0, 0];
    this.center = center || [0, 0, 0];

    this.parameters = {
      width: width,
      height: height,
      depth: depth,
      widthSegments: widthSegments,
      heightSegments: heightSegments,
      depthSegments: depthSegments
    };

    const colorsSide = {
      front: [255, 255, 255, 255],
      left: [255, 255, 255, 255],
      top: [255, 255, 255, 255],
      right: [255, 255, 255, 255],
      bottom: [255, 255, 255, 255],
      back: [255, 255, 255, 255],
    };

    if (color) {
      for (let c in colorsSide) {
        colorsSide[c] = color;
      }
    }
    else if (!texture) {
      for (let c in colorsSide) {
        colorsSide[c] = WebGLerPrimitives.randomColor();
      }
    }

    let numberOfVertices = 0;

    const buffers = WebGLerPrimitives.identifyBuffers();

    numberOfVertices = WebGLerPrimitives.buildSide('x', 'y', 'z', width, height, 0, widthSegments, heightSegments, numberOfVertices, buffers, colorsSide.front, 1); // front
    numberOfVertices = WebGLerPrimitives.buildSide('y', 'x', 'z', height, width, depth, widthSegments, heightSegments, numberOfVertices, buffers, colorsSide.back, -1); // back
    numberOfVertices = WebGLerPrimitives.buildSide('y', 'z', 'x', height, depth, 0, depthSegments, heightSegments, numberOfVertices, buffers, colorsSide.left, -1); // left
    numberOfVertices = WebGLerPrimitives.buildSide('z', 'y', 'x', depth, height, width, depthSegments, heightSegments, numberOfVertices, buffers, colorsSide.right, 1); // right
    numberOfVertices = WebGLerPrimitives.buildSide('z', 'x', 'y', depth, width, 0, widthSegments, depthSegments, numberOfVertices, buffers, colorsSide.top, 1); // top
    numberOfVertices = WebGLerPrimitives.buildSide('x', 'z', 'y', width, depth, height, widthSegments, depthSegments, numberOfVertices, buffers, colorsSide.bottom, -1); // bottom

    const uniforms = {
      u_matrix: glMatrix[this._gl.matrixType].create()
    };

    this.setGLVariables(gl, programInfo, buffers, uniforms, primitiveType);
    this.resetParameters(options);
  }
}

export class Sphere extends WebGLerPrimitive3d {
  constructor(gl, programInfo, options) {
    super(options);

    const {
      primitiveType,

      position,
      center,
      color,
      texture,

      radius = 1,
      phiStart = 0,
      phiLength = Math.PI * 2,
      thetaStart = 0,
      thetaLength = Math.PI
    } = options;

    const widthSegments = Math.max(3, Math.floor(options.widthSegments) || 8);
    const heightSegments = Math.max(2, Math.floor(options.heightSegments) || 6);

    this.position = position || [0, 0, 0];
    this.center = center || [0, 0, 0];

    this.parameters = {
      radius,
      widthSegments,
      heightSegments,
      phiStart,
      phiLength,
      thetaStart,
      thetaLength
    };

    const buffers = WebGLerPrimitives.identifyBuffers();

    const defaultColor = !texture ? color : [255, 255, 255, 255];

    const latRange = thetaLength - thetaStart;
    const longRange = phiLength - phiStart;

    for (let y = 0; y <= heightSegments; y++) {
      let colorSegment = defaultColor || WebGLerPrimitives.randomColor();

      const v = y / heightSegments;
      const phi = latRange * v;
      const sinPhi = Math.sin(phi);
      const cosPhi = Math.cos(phi);

      for (let x = 0; x <= widthSegments; x++) {
        const u = x / widthSegments;
        const theta = longRange * u;
        const sinTheta = Math.sin(theta);
        const cosTheta = Math.cos(theta);

        let vertex = {};
        vertex.x = cosTheta * sinPhi;
        vertex.y = cosPhi;
        vertex.z = sinTheta * sinPhi;

        buffers.positions.data.push(radius * vertex.x, radius * vertex.y, radius * vertex.z);

        buffers.normals.data.push(vertex.x, vertex.y, vertex.z);

        buffers.uvs.data.push(u, 1 - v);

        buffers.colors.data.push(colorSegment[0], colorSegment[1], colorSegment[2], colorSegment[3]);
      }
    }

    const numVertsAround = widthSegments + 1;
    for (let x = 0; x < widthSegments; x++) {
      for (let y = 0; y < heightSegments; y++) {
        const a = y * numVertsAround + x;
        const b = (y + 1) * numVertsAround + x;

        buffers.indices.data.push(a, a + 1, b);
        buffers.indices.data.push(b, a + 1, b + 1);
      }
    }

    const uniforms = {
      u_matrix: glMatrix[this._gl.matrixType].create(),
    };

    this.setGLVariables(gl, programInfo, buffers, uniforms, primitiveType);
    this.resetParameters(options);
  }
}

export class Cylinder extends WebGLerPrimitive3d {
  constructor(gl, programInfo, options) {
    super(options);

    const {
      primitiveType,

      position,
      center,
      color,
      texture,

      radiusTop = 1,
      radiusBottom = 1,
      height = 1,
      openEnded = false,
      thetaStart = 0,
      thetaLength = Math.PI * 2,
    } = options;

    const radialSegments = Math.max(3, Math.floor(options.radialSegments) || 8);
    const heightSegments = Math.max(1, Math.floor(options.heightSegments) || 1);

    this.position = position || [0, 0, 0];
    this.center = center || [0, 0, 0];

    this.parameters = {
      radiusTop: radiusTop,
      radiusBottom: radiusBottom,
      height: height,
      radialSegments: radialSegments,
      heightSegments: heightSegments,
      openEnded: openEnded,
      thetaStart: thetaStart,
      thetaLength: thetaLength
    };

    let index = 0;
    const halfHeight = height / 2;

    const buffers = WebGLerPrimitives.identifyBuffers();

    let topColor = [];
    let bottomColor = [];
    generateTorso();

    if (!openEnded) {
      if (radiusTop > 0) generateCap(true, bottomColor);
      if (radiusBottom > 0) generateCap(false, topColor);
    }

    const uniforms = {
      u_matrix: glMatrix[this._gl.matrixType].create()
    };

    this.setGLVariables(gl, programInfo, buffers, uniforms, primitiveType);
    this.resetParameters(options);

    function generateTorso() {
      let indexArray = [];

      const slant = Math.atan2(radiusTop - radiusBottom, height);
      const cosSlant = Math.cos(slant);
      const sinSlant = Math.sin(slant);

      const defaultColor = !texture ? color : [255, 255, 255, 255];

      for (let y = 0; y <= heightSegments; y++) {
        let colorSegment = defaultColor || WebGLerPrimitives.randomColor();
        if (y === 0) {
          topColor = colorSegment;
        }
        else if (y === heightSegments) {
          bottomColor = colorSegment;
        }

        let indexRow = [];

        const v = y / heightSegments;
        const ys = v * height - halfHeight;
        const radius = (radiusBottom - radiusTop) * v + radiusTop;

        for (let x = 0; x <= radialSegments; x++) {
          const u = x / radialSegments;
          const theta = thetaStart + u * thetaLength;

          const sinTheta = Math.sin(theta);
          const cosTheta = Math.cos(theta);

          let vertex = {};
          vertex.x = radius * cosTheta;
          vertex.y = ys;
          vertex.z = radius * sinTheta;

          buffers.positions.data.push(vertex.x, vertex.y, vertex.z);

          vertex.x = cosTheta * sinSlant;
          vertex.y = cosSlant;
          vertex.z = sinTheta * sinSlant;

          buffers.normals.data.push(vertex.x, vertex.y, vertex.z);

          buffers.uvs.data.push(u, v);

          buffers.colors.data.push(colorSegment[0], colorSegment[1], colorSegment[2], colorSegment[3]);

          indexRow.push(index++);
        }

        indexArray.push(indexRow);
      }

      for (let x = 0; x < radialSegments; x++) {
        for (let y = 0; y < heightSegments; y++) {
          const a = indexArray[y][x];
          const b = indexArray[y + 1][x];
          const c = indexArray[y + 1][x + 1];
          const d = indexArray[y][x + 1];

          buffers.indices.data.push(a, b, d);
          buffers.indices.data.push(b, c, d);
        }
      }
    }

    function generateCap(top, color) {
      let centerIndexStart = index;
      let centerIndexEnd;

      const radius = top ? radiusTop : radiusBottom;
      const sign = top ? -1 : 1;

      for (let x = 1; x <= radialSegments; x++) {
        let colorSegment = color || WebGLerPrimitives.randomColor();

        buffers.positions.data.push(0, halfHeight * sign, 0);

        buffers.normals.data.push(0, sign, 0);

        buffers.uvs.data.push(0.5, 0.5);

        buffers.colors.data.push(colorSegment[0], colorSegment[1], colorSegment[2], colorSegment[3]);

        index++;
      }
      centerIndexEnd = index;

      for (let x = 0; x <= radialSegments; x++) {
        let colorSegment = color || WebGLerPrimitives.randomColor();

        let u = x / radialSegments;
        let theta = thetaStart + u * thetaLength;

        let cosTheta = Math.cos(theta);
        let sinTheta = Math.sin(theta);

        let vertex = {};
        vertex.x = radius * cosTheta;
        vertex.y = halfHeight * sign;
        vertex.z = radius * sinTheta;
        buffers.positions.data.push(vertex.x, vertex.y, vertex.z);

        buffers.colors.data.push(colorSegment[0], colorSegment[1], colorSegment[2], colorSegment[3]);

        buffers.normals.data.push(0, sign, 0);

        let uv = {};
        uv.x = -(sinTheta * 0.5) + 0.5;
        uv.y = (cosTheta * 0.5 * sign) + 0.5;
        buffers.uvs.data.push(uv.x, uv.y);

        index++;
      }

      for (let x = 0; x < radialSegments; x++) {
        let c = centerIndexStart + x;
        let i = centerIndexEnd + x;

        if (top === true) {
          buffers.indices.data.push(i, i + 1, c);
        }
        else {
          buffers.indices.data.push(i + 1, i, c);
        }
      }
    }
  }
}