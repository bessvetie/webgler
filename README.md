## Getting Started

`npm install` - Install dependencies.

`npm run build-dev` - Run development build with hot reload. If it doesn't start, make sure you aren't running anything else in the same port. 

**Default PORT=3300**

`npm run build-prod` - Run production build. See public folder.