module.exports = {
  plugins: [
    require('postcss-partial-import'),
    require('postcss-simple-vars'),
    require('postcss-nested'),
    require('postcss-mixins'),
    require('autoprefixer')
  ]
}