switch(process.env.NODE_ENV) {
  case 'production':
    module.exports = require('./conf/webpack.production.config');
    break;
  case 'development':
  default:
    module.exports = require('./conf/webpack.development.config');
}