const express = require('express');
const path = require('path');

const webpack = require('webpack');
const webpackConfig = require('./webpack.config');

const PORT = webpackConfig.devServer.port;
const app = express();

const isDevelopment = process.env.NODE_ENV === 'development';

if (isDevelopment) {
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');

  const compiler = webpack(webpackConfig);

  app.use(webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath,
    noInfo: true,
    hot: true,
    stats: {
      colors: true
    }
  }));
  app.use(webpackHotMiddleware(compiler));

  commonUse();
}
else {
  const PUBLIC_PATH = path.resolve(__dirname, 'public');

  app.use(express.static(PUBLIC_PATH));

  commonUse();

  app.all("*", function(req, res) {
    res.sendFile(path.resolve(PUBLIC_PATH, 'index.html'));
  });
}

function commonUse() {
  app.use('/images', express.static('src/demo/images'));
  app.use('/shaders', express.static('src/shaders'));
}

app.listen(PORT, function() {
  console.log('Listening on port ' + PORT + '...');
});